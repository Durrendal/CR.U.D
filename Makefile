LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
LUA_ROCKS ?= /usr/bin/luarocks-5.3
DESTDIR=/usr/bin

fetch-libraries:
	$(LUA_ROCKS) install lume --local

compile-bin:
	cd ./src/ && fennel --compile-binary crud.fnl tcrud-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install ./src/crud-bin -D $(DESTDIR)/crud
