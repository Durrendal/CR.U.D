#!/usr/bin/fennel
;;CR.U.D
;;Authors:
;;Will Sinatra <wpsinatra@gmail.com>
;;Jesse Laprade <jesselaprade@gmail.com>

;;===== Depends =====
(local lume (require :lume))
(local ffs (require :ffs))
(local praint (require :praint))

;;===== Globals =====
;;Exposed System Config
(global settings_dir "/etc/CR.U.D/")
(global CRUD_config (.. settings_dir "CR.U.D.conf"))
;;User dotfile
(global cnffile (.. (os.getenv "HOME") "/.cr.u.d"))

;;Global Configurations
(var conftbl {})
;;User Configurations
(fn get-editor-fallback []
  (let [env-editor (os.getenv "EDITOR")]
    (if env-editor
        env-editor
        "nano")))

(fn get-editor []
  (let [env-visual (os.getenv "VISUAL")]
    (if env-visual
        env-visual
        (get-editor-fallback))))

(var user_settings {"editor" (get-editor)
                    "username" (os.getenv "USER")})

;;===== Util =====
(fn util/split [val check]
    (if (= check nil)
        (do
         (local check "%s")))
    (local t {})
    (each [str (string.gmatch val (.. "([^" check "]+)"))]
          (do
           (table.insert t str)))
    t)

(fn util/exists? [f]
  (local f? (io.open f "r"))
  (if f?
      (f?:close)
      false))

(fn util/mitigate []
  (local valid_vars ["editor" "username"])
  (local valid_holds ["$EDITOR" (os.getenv "USER")])
  (var reset nil)
  (each [k v (pairs valid_vars)]
    (do
      (if (= (. conftbl v) nil)
          (do
            (tset conftbl v (. valid_holds k))
            (set reset true)))))
  (if (= reset true)
      (with-open [f (io.open cnffile "w")]
        (f:write (lume.serialize conftbl)))))

(fn util/conf []
  (if (util/exists? cnffile)
      (do
        (with-open [f (io.open cnffile "r")]
          (set conftbl (lume.deserialize (f:read "*a"))))
        (util/mitigate))
      (with-open [f (io.open cnffile "w")]
        (f:write (lume.serialize conftbl)))))

;;===== Data =====
;;Thread Data Format
;;{
;; thread="Title of Thread",
;; author=(osgetenv "USER")
;; created="YYYY-MM-DD",
;; modified="YYYY-MM-DD",
;; comments={
;;           {"Comment A", author=(os.getenv "USER")},
;;           {"Comment B", author=(os.getenv "USER")}
;;  },
;;}

;;===== Interface =====

;;===== Main =====
