#!/usr/bin/env lua
-- ffs - A Fennel and Lua library for handling files and directories
-- Author: Jesse Laprade (m455)
-- License: AGPL3 (https://www.gnu.org/licenses/agpl-3.0.en.html)
-- Source: https://git.m455.casa/m455/ffs
local ffs = {}
ffs["shell->sequence"] = function(command)
  local file_handle = io.popen(command, "r")
  local function close_handlers_0_(ok_0_, ...)
    file_handle:close()
    if ok_0_ then
      return ...
    else
      return error(..., 0)
    end
  end
  local function _0_()
    local tbl_0_ = {}
    for i in file_handle:lines() do
      tbl_0_[(#tbl_0_ + 1)] = i
    end
    return tbl_0_
  end
  return close_handlers_0_(xpcall(_0_, (package.loaded.fennel or debug).traceback))
end
ffs["directory-exists?"] = function(directory)
  local command = string.format("test -d %s && echo exists", directory)
  if (ffs["shell->sequence"](command))[1] then
    return true
  else
    return false
  end
end
ffs["directory-create"] = function(dir)
  return os.execute(string.format("mkdir -p %s", dir))
end
ffs["directory-contents"] = function(dir)
  return ffs["shell->sequence"](string.format("ls %s", dir))
end
ffs["file-exists?"] = function(file)
  local command = string.format("test -f %s && echo exists", file)
  if (ffs["shell->sequence"](command))[1] then
    return true
  else
    return false
  end
end
ffs["file-create"] = function(file)
  return io.close(io.open(file, "w"))
end
ffs["file-write"] = function(file, data, mode)
  local file_out = io.open(file, mode)
  local function close_handlers_0_(ok_0_, ...)
    file_out:close()
    if ok_0_ then
      return ...
    else
      return error(..., 0)
    end
  end
  local function _0_()
    return file_out:write(data)
  end
  return close_handlers_0_(xpcall(_0_, (package.loaded.fennel or debug).traceback))
end
ffs["file->lines"] = function(file)
  local tbl_0_ = {}
  for line in io.lines(file) do
    tbl_0_[(#tbl_0_ + 1)] = line
  end
  return tbl_0_
end
ffs["path-exists?"] = function(path)
  return (ffs["file-exists?"](path) or ffs["directory-exists?"](path))
end
ffs["path-copy"] = function(source, dest)
  return os.execute(string.format("cp -r %s %s", source, dest))
end
ffs["path-delete"] = function(path)
  return os.execute(string.format("rm -rf %s", path))
end
ffs["paths-missing"] = function(mode, required_paths)
  local func = nil
  do
    local _0_0 = mode
    if (_0_0 == "files") then
      func = ffs["file-exists?"]
    elseif (_0_0 == "directories") then
      func = ffs["directory-exists?"]
    else
    func = nil
    end
  end
  local tbl_0_ = {}
  for _, path in ipairs(required_paths) do
    local _1_
    if not func(path) then
      _1_ = path
    else
    _1_ = nil
    end
    tbl_0_[(#tbl_0_ + 1)] = _1_
  end
  return tbl_0_
end
return ffs
