#!/usr/bin/env lua
-- praint - A Fennel and Lua library for print colours to the terminal
-- Author: Jesse Laprade (m455)
-- License: AGPL3 (https://www.gnu.org/licenses/agpl-3.0.en.html)
-- Source: https://git.m455.casa/m455/praint
local praint = {}
local escape = (string.char(27) .. "[")
local styles = {bold = 1, default = 0}
local colours = {black = 30, blue = 34, cyan = 36, default = 39, green = 32, magenta = 35, red = 31, white = 37, yellow = 33}
local function styles_ref(styles_key)
  return styles[styles_key]
end
local function colours_ref(colours_key)
  return colours[colours_key]
end
local function render(escape_code)
  local code = nil
  if ("table" == type(escape_code)) then
    code = (tostring(escape_code[1]) .. ";" .. tostring(escape_code[2]))
  else
    code = tostring(escape_code)
  end
  return (escape .. code .. "m")
end
praint.paint = function(string, colour_key, style_key)
  local colour = colours_ref(colour_key)
  local style = nil
  if not style_key then
    style = styles_ref("default")
  else
    style = styles_ref(style_key)
  end
  local colour_default = colours_ref("default")
  local style_default = styles_ref("default")
  return (render({style, colour}) .. string .. render({style_default, colour_default}))
end
praint.praint = function(string, colour_key, style_key)
  return print(praint.paint(string, colour_key, style_key))
end
return praint
